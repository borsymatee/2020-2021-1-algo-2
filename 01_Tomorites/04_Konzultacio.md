# 4. hét - 2020. 09. 30. - Konzultáció a tömörítés témában

* Feladatmegoldások Nagy Ádám anyaga alapján

## Alapok és naiv tömörítés


* Input:
```
s = AABCAADEAAB
```

### 1. Naiv módszert alkalmazva hány bittel tudjuk ábrázolni az s-t?

* 5 féle karakter van, amit 3 biten tárolhatunk, mert 2^3 = 8, ami nem kisebb, mint 5
* 11 betű van: 11*3 = 33 bit lesz a kódhossz
* Példa megfeleltetés:

```
A   000
B   001
C   010
D   100
E   101
```

### 2. Adjuk meg s egy kódolt alakját naiv módszer esetén

* Szóban megadtuk :)

### 3. Adjunk meg betűnkénti kódolást, amely tömörebb eredményt ad, mint a naiv módszer!

* Betűnkénti: a lényeg, hogy megint egy betűhöz egy kódot rendelünk, és konzekvensen egy bizonyos betűhöz mindig ugyanazt a kódot
* Arról viszont nincs szó, hogy a kódoknak egyforma hosszúnak kell lenniük
* Ugyanakkor, arra vigyázni kell, hogy egyértelműen dekódolható legyen az eredmény, tehát pl. legyen prefixmentes a kód
* Pl.:

```
A   0
B   100
C   101
D   110
E   111
```

* Itt az A-ból 6 db van, ezért 6 bit
* A többi 3 biten van és összesen 5 db van belőlük, ezért azok 15 bitet foglalnak
* Összesen 21 bit

### 4. Adjunk meg egy egyenletes kódolást, amely tömörebb eredményt ad, mint a naiv módszer!

* Egyszerű, de nagyszerű ötlet, ha ugyanazt csináljuk, mint az 1. feladatnál, de ne az "A", hanem az "AA" legyen a "000"
* Hiszen az összes A párban van ebben a konkrét inputban, tehát az A-kat így feleannyi biten tároljuk, de a többi karakteren sem bukunk
* Persze attól még, hogy erre a konkrét inputra optimalizálunk, azért más, az adott ábécé feletti kóddal is jó ha elbírunk, ezért a sima A-hoz is rendeljünk valamit
* Az "egyenletesség" azt jelenti, hogy a kódszavak hossza azonos, ezért legyen pl. ez az 111
* Ekkor a dekódolás egyértelmű (mivel azonosak a kódhosszak, nincs többértelműség), és az enkódolás se problémás, A esetében legyen az a szabály, ha párosával van, a 000-t használjuk, ha páratlanul, akkor a maradékra az 111-et
* Fontos látni, ez nem naiv és nem is valamelyik másik nevezetes kódolás, hanem tetszőleges

### 5. Rajzoljuk fel a korábban használt kód kódfáját! Milyen tulajdonsággal rendelkezik egy egyenletes kódhoz tartozó kódfa?

* Azaz ennek:

```
A   111
AA  000
B   001
C   010
D   100
E   101
```

* Ez nem egy Huffman-kódfa, csak egy random fa, nem is kell, hogy bináris legyen
* Az élcímkézés se kötelezően olyan, mint a Huffmannál, ezért itt most ténylegesen rá is írjuk, hogy tudjuk
* A gyökér az üres kódszó, ebből kiindulva olvasunk prefixeket (épp, mint a Huffmannál), és így érünk el a kódszavakat reprezentáló csúcsokig - amik nem szükségképpen a levelek

```

                 ""
                /  \
               /    \
             0/      \1
             /        \
          "0"          "1"
          / \          / \
        0/   \1      1/   \0
        /     \      /     \
     "00"    "01"  "11"    "10"
     /  \      |     |      /  \
   0/    \1    |0    |1   0/    \1
"000"  "001" "010" "111"  "100" "101"
 (AA)   (B)    (C)  (A)    (D)   (E)
```

* Egyenletes kód kódfájának tulajdonságai:
    * Minden levél ugyanazon a szinten van (mégse nevezném teljes fának, mert maradhatnak ki ágak, nem is feltétlen bináris)
    * Minden kódszót reprezentáló csúcs egyben levél is

## Huffman-kódolás

### 1. A mintához hasonlóan végezzük el a Huffman-kódolást egy általunk (általam) választott tetszőleges szöveg esetén!

* A változatosság gyönyörködtet:

```
AABCAADEAAB
```

* Gyakorisástábla:

```
A   6
B   2
C   1
D   1
E   1
```

* Ellenőrizzük, 11 a sorok összege, és valóban, ennyi karakter van

```
   C1    D1
    \   /
     \ /
E1    2
 \   /
  \ /
   3  B2
    \/
A6  5
 \  /
  11
```

* Érdekes, amikor az E-t dolgoztuk fel (2. lépés) azt akár a B-vel is vehettük volna, nem kellett volna feltétlen a C-D közös csúccsal. A Huffmannál nincs kőbe vésve, melyiket kell választani, elvileg azonos kódolási aránynak kell kijönnie minden esetben
* Kódok:

```
A: 0
B: 11
C: 1010
D: 1011
E: 100
```

* Eredmény:

```
A: 1 * 6
B: 2 * 2
C: 4 * 1
D: 4 * 1
E: 3 * 1
```

* Azaz: 21 bit + fa
* Ugyanúgy 21 bit, mint az előző rész 3. feladatánál, kb. az lett volna az eredmény, ha a nemdeterminisztikus résznél a másik párral vesszük fel az E-t

### 2. Keressünk olyan szöveget, ahol a Huffman-kódolás pontosan annyi bittel történik mint a naiv módszerrel!

* Példák: A, AB, ABCDEFGH, AAABBBCCCDDD
* Arra jutottunk, hogy minden karakternek azonos létszámban kell jelen lennie, illetve 2 hatványnak kell lennie a teljes hossznak, hogy a Huffman-fa egy teljes fa legyen

### 3. Írjunk programot (struktogramot), amely a bemenetként kapott kódolt szöveg és hozzá tartozó Huffman-kód kódfája alapján helyreállítja az eredeti szöveget!

* Kihagytuk, mert kódolós szorgalmi lesz (lásd a szorgalmi kiírások között)

### 4. Írjuk meg a Huffman-kódolást megvalósító programot. Bemenet egy szöveg, kimenet a kódolt szöveg és a kódfa!

* Kihagytuk, mert kódolós szorgalmi lesz (lásd a szorgalmi kiírások között)

### 5. Számoljuk ki a ternáris (r = 3) Huffman-kódot a korábbi példára!

* Kihagytuk, mert kisszorgalmi lesz (lásd a szorgalmi kiírások között)

### 6. Verseny a csoportban: Keressük azt a 10 hosszú szöveget, amelyet a legnagyobb mértékben lehetett tömöríteni a minimális szóhosszt használó naiv módszerhez képest (Huffman-kóddal)

* Három pályamű érkezett, bár a legutolsó lapzárta után

#### Első versenyző

```
ABCDEABCDE
```

* Kódfa:

```
A   B C   D
 \ /   \ /
  4     4   E
   \     \ /
    \     6
     \   /
      \ /
       10
```
* Kódhosszak:

```
A 2*2
B 2*2
C 3*2
D 3*2
E 2*2
```

* 30 vs 24, plusz a fa
* Nem az igazi, mert bár nem 2 hatvány az ábécé hossza, tehát nem spórol a naiv módszer, de midnenből ugyanannyi van, így nem jön ki a Huffman haszna

#### 2. versenyző

```
AAAAAAAABC
```

* Itt a naiv 20 bites, hiszen 10 karakter van, de a C miatt már két bitet igényel
* A Huffman pedig 12, mert az A-nak egy hosszú ágat tudunk csinálni (8*1 bit), a B-nek és C-nek pedig 2 hosszút, ami így összesen 8 + 2 + 2 bit
* Plusz persze a fa
* Itt érezzük, ha több A lenne, ez mégjobb lenne

#### 3. versenyző

```
ABCDEEEEEE
```

* Hasonló, mint az előző, itt is épp, hogy már kell a következő bit, de épp emiatt a Huffmannak van lehetősége egy biten is tárolni, amit jól esik neki
* Tehát 5 féle karakter, ami miatt 3 bit kell már
* 10 hosszú a string, ezért naivul 30 bit kell
* Huffman-fa:

```
A  B  C   D
 \ /   \ /
  2     2
   \   /
    \ /
     4   E
      \ /
       10
```

* Értékek:

```
Kód Hossz Db  Össz
A   3     1   3
B   3     1   3
C   3     1   3
D   3     1   3
E   1     6   6
SUM           18 bit
```

* Érdekes, ez pont ugyanaz az arány, mint az előbb, de itt is hasonlóan, az E-k számának növelésével a végtelenségig lehet nyitni az ollót a két módszer között
